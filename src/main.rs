//const MAX: usize = 100;
//
//
use std::{time::Instant, fs, io::Write};
fn main() {
	let args: Vec<String> = std::env::args()
		.collect();

	let maxim = args.get(1)
		.expect("T'has oblidat de dir-me el nombre màxim")
		.parse()
		.expect("Argument incorrecte");

	let mut no_primers = Vec::new();

    // Marcant no primeres
    let comença_a_calcular_no_primers = Instant::now();
	for n in 2..maxim {
	    for mult in 2..maxim{
	    	if n*mult >= maxim {break;}
	    	no_primers.push(n*mult);
	    }
	}

    let temps_per_trobar_no_primers = Instant::now().duration_since(comença_a_calcular_no_primers).as_secs(); // Totos els temps son per veure quant ha tardat

    let mut primers = (1..maxim).rev().collect::<Vec<usize>>();
    treure_repetits(&mut no_primers);

    let comença_a_eliminar_los = Instant::now();

    for (i, p) in no_primers.into_iter().enumerate() {
        primers.remove(primers.len() + i - p);
    }
    primers.remove(primers.len() - 1); // Traiem també l'1, que no és ni primer ni composit

    let temps_per_eliminar_los = Instant::now().duration_since(comença_a_eliminar_los).as_secs();

    primers = primers.into_iter().rev().collect();

    // Informacio extra
	println!("Trobar els no-primers ha costat {:?} segons", temps_per_trobar_no_primers);
	println!("Eliminar-los ha costat {:?} segons", temps_per_eliminar_los);

    // Guardem sortida al fitxer
    let nom_fitxer = format!("{}.data", maxim);
    let mut fitxer = fs::File::create(nom_fitxer).expect("No s'ha pogut crear el fitxer per guardar-lo");

    let continguts = {
        let mut tmp: String = String::new();
        for primer in primers {
            tmp.push_str(&format!("{}, ", primer)[..]);
        }
        tmp[..tmp.len()-2].to_string() // Evita la coma i l'espai final
    };
    fitxer.write_all(continguts.as_bytes()).expect("No s'ha pogut guardar el fitxer final");
}

fn treure_repetits(v: &mut Vec<usize>) {
    v.sort();
    v.dedup();
}
